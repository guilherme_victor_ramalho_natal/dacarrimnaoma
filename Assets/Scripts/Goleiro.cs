﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goleiro : MonoBehaviour {

    public Atacante enemyNeymar;
    public float jumpTime;
    public float movSpeed;
    public float limits;
    public float ballPosThreshold;
    public float maxScale;
    public Sprite upSprite;
    public Sprite fallenSprite;

    float elapsedTime;
    bool jumping;
    bool fallen;
	public Atacante atacante;

    new Collider2D collider;
    SpriteRenderer spriteRenderer;

    void Awake() {
        collider = GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update() {
        if (!jumping && !fallen) {
            if (Ball.instance.position.y < limits && Ball.instance.position.y > -limits) {
                if (Mathf.Abs(Ball.instance.position.y - transform.position.y) > ballPosThreshold)
                    jumping = true;
            }
        }
    }

    void FixedUpdate() {
        if (jumping && !fallen) {
            elapsedTime += Time.fixedDeltaTime;
            VerticalMove();
            Jump();
            if (elapsedTime >= jumpTime)
                FinishJump();
        }
    }

    void VerticalMove() {
        var position = transform.position;
        position.y = Mathf.Lerp(position.y, Ball.instance.position.y, movSpeed);
        position.y = Mathf.Clamp(position.y, -limits, limits);
        transform.position = position;
    }

    void Jump() {
        float percentage = elapsedTime / jumpTime;
        float currentScale = Mathf.Lerp(1, maxScale, (1 - (percentage * 2 - 1) * (percentage * 2 - 1)));
        transform.localScale = new Vector3(currentScale, currentScale, 1);
    }

    void FinishJump() {
        transform.localScale = new Vector3(1, 1, 1);
        elapsedTime = 0;
        jumping = false;
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (!fallen) {
            var playerInput = collision.gameObject.GetComponent<PlayerInput>();
            if (playerInput) {
                if (collision.gameObject.tag != gameObject.tag && playerInput.state == PlayerState.CARRINHO) {
                    fallen = true;
                    collider.isTrigger = true;

                    if ((tag == "Time1" ? ConeMovement.vivo1 : ConeMovement.vivo2) > 0){
                        Judge.instance.setTarget(collision.rigidbody.position);
                        playerInput.lifebar.UpdateLife();
                        playerInput.JudgeComing();
                        print("asd para po");
                    } else {
                        enemyNeymar.IncreaseVelocity();
						atacante.canDie = true;
                    }
                    spriteRenderer.sortingOrder = 0;
                    spriteRenderer.sprite = fallenSprite;
                    CameraShake.instance.Shake();
                }
            }
            else if (collision.gameObject == Ball.instance.gameObject) {
                Vector2 targetPosition = new Vector2(Random.Range(-2, 2), Random.Range(-6, 6));
                Ball.instance.ReceiveKick(targetPosition, 2);
            }
        }
    }

    public void GetUp(){
        if ((gameObject.tag == "Time1" ? ConeMovement.vivo1 : ConeMovement.vivo2) > 0){
            spriteRenderer.sprite = upSprite;
            spriteRenderer.sortingOrder = 1;
            fallen = false;
            collider.isTrigger = false;
        }
    }
}
