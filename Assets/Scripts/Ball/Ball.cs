﻿using UnityEngine;

public class Ball : MonoBehaviour {

    public static Ball instance { get; private set; }

    public bool grounded { get; private set; }

    public float maxScale;

    new Rigidbody2D rigidbody;
    float airTime;
    float elapsedTime;
    float startingdrag;

	public bool isTouchingNeymar;

	public string ballPossesion;

    public Vector2 position {
        get {
            return rigidbody.position;
        }

        set {
            rigidbody.MovePosition(value);
        }
    }

	void Awake () {
        if (instance)
            Debug.LogError("Cant have more than one ball");
        instance = this;
        rigidbody = GetComponent<Rigidbody2D>();
        startingdrag = rigidbody.drag;
	}

    void FixedUpdate() {
        if (!grounded) {
            elapsedTime += Time.fixedDeltaTime;
            UpdateScale();
            if (elapsedTime >= airTime) {
                grounded = true;
                GetComponent<Collider2D>().isTrigger = false;
                GetComponent<SpriteRenderer>().sortingOrder = 1;
                transform.localScale = new Vector3(1, 1, 1);
                rigidbody.drag = startingdrag;
            }
        }
    }

    void UpdateScale() {
        float percentage = elapsedTime / airTime;
        float currentScale = Mathf.Lerp(1, maxScale, (1 - (percentage * 2 - 1) * (percentage * 2 - 1)));
        transform.localScale = new Vector3(currentScale, currentScale, 1);
    }

    public void ReceiveKick(Vector2 targetPosition, float time) {
        GetComponent<SpriteRenderer>().sortingOrder = 4;
        GetComponent<Collider2D>().isTrigger = true;
        rigidbody.velocity = (targetPosition - position) / time;
        rigidbody.drag = 0;
        elapsedTime = 0;
        airTime = time;
        grounded = false;
    }

	void OnCollisionEnter2D(Collision2D collision){
		ballPossesion = collision.gameObject.tag;
	}


}
