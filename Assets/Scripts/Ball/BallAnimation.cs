﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallAnimation : MonoBehaviour {

	public Animator animator;
	public Ball ball;

	void Update () {
		if (GetComponent<Rigidbody2D> ().velocity.magnitude > 0.3f)
			animator.SetBool ("moving",true);
		else
			animator.SetBool ("moving",false);
	}
}
