﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Victory : MonoBehaviour {

    public GameObject victory1;
    public GameObject victory2;
    public NextScene next;

    void Start () {
		var team = PlayerPrefs.GetString ("Vitoria");
        if (team == "player1") {
            victory1.SetActive(true);
            victory2.SetActive(false);
        }
        else {
            victory1.SetActive(false);
            victory2.SetActive(true);
        }
	}

    void Update() {
        if (Input.anyKeyDown)
            next.LoadNextScene();
    }
}
