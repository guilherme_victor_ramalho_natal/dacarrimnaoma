﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    public static CameraShake instance { get; private set; }

    public float defaultIntensity;
    public float defaultDuration;
    public bool active;

    float intensity;
    float duration;
    float elapsedTime;

    public void Shake() {
        Shake(defaultDuration, defaultIntensity);
    }

    public void Shake(float duration, float intensity) {
        this.intensity = intensity;
        this.duration = duration;
        active = true;
        elapsedTime = 0;
    }

    void Awake() {
        if (instance)
            Debug.LogError("Multiple camera shakes not supported");
        instance = this;
    }

    void Update() {
        if (active) {
            elapsedTime += Time.deltaTime;
            transform.position = new Vector3(Random.Range(-intensity, intensity), Random.Range(-intensity, intensity), transform.position.z);
            if (elapsedTime > duration) {
                elapsedTime = 0;
                active = false;
                transform.position = new Vector3(0, 0, transform.position.z);
            }
        }
    }
}
