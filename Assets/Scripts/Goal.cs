﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour {

	public NextScene next;
	public string scene;
    public GameObject goalMessage1;
    public GameObject goalMessage2;
    public float waitTime;

    bool ended;

	void Start(){
		next.scene = scene;

	}

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.gameObject.tag == "Ball" && !ended) {
			if (Ball.instance.isTouchingNeymar) {
				if (Ball.instance.ballPossesion == "Time1") {
					PlayerPrefs.SetString ("Vitoria","player1");
                    goalMessage1.SetActive(true);
                }
				else {
					PlayerPrefs.SetString ("Vitoria","player2");
                    goalMessage2.SetActive(true);
                }
                Invoke("LoadVictoryScene", waitTime);
                ended = true;
			}
		}
	}

    void LoadVictoryScene() {
        next.LoadNextScene();
    }
}
