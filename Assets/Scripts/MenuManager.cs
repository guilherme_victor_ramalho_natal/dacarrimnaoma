﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    Animator animator;

    void Awake() {
        animator = GetComponent<Animator>();
    }

	void Update () {
		if (animator.GetCurrentAnimatorStateInfo(0).IsName("Instructions End")) {
            if (Input.anyKeyDown)
                animator.SetTrigger("backtomenu");
        }
	}
}
