﻿using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static SoundManager instance { get; private set; }

    public List<AudioSource> sonsDeFalta = new List<AudioSource>();
    public List<AudioSource> sonsDeGol = new List<AudioSource>();
    public List<AudioSource> sonsDeStun= new List<AudioSource>();
    public AudioSource somRedeGlobo;
    public AudioSource somFimDeJogo;

    void Awake() {
        if (instance)
            DestroyImmediate(gameObject);
        else {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
	}

    public void AtivaSomFalta() {
        sonsDeFalta[Random.Range(0, sonsDeFalta.Count - 1)].Play();
    }

    public void AtivaSomGol() {
        sonsDeGol[Random.Range(0, sonsDeGol.Count - 1)].Play();
    }

    public void AtivaSomStun() {
        sonsDeStun[Random.Range(0, sonsDeStun.Count - 1)].Play();
    }

    public void AtivaSomRedeGlobo() {
        somRedeGlobo.Play();
    }

    public void AtivaSomFimDeJogo() {
        somFimDeJogo.Play();
    }
}
