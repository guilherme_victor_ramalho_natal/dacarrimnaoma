﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeMovement : MonoBehaviour {

	public Atacante targetNeymar;
	public bool isFallen;

    void Start() {
        Obstacles.instance.AddObstacle(transform);
    }

    public static int vivo1 = 0, vivo2 = 0;
	void OnCollisionEnter2D(Collision2D collider){
		if (collider.gameObject.tag != gameObject.tag && collider.gameObject.tag.Contains("Time")) {
			if (collider.gameObject.GetComponent<PlayerInput> ().state == PlayerState.CARRINHO) {
				GetComponent<Collider2D> ().isTrigger = true;
				CameraShake.instance.Shake ();
				targetNeymar.IncreaseVelocity ();
				isFallen = true;
                Obstacles.instance.RemoveObstacle(transform);
                if (gameObject.tag == "Time1"){
                    vivo1--;
                } else {
                    vivo2--;
                }
			}
		}
	}




}