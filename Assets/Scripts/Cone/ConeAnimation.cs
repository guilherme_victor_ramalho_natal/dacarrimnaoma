﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeAnimation : MonoBehaviour {

	public ConeMovement cone;
	public Animator animator;



	void Start(){
		animator.Update (Random.Range(0,8));
	}

	void Update () {
		if (cone.isFallen) {
			animator.SetBool ("Fallen",cone.isFallen);
		}
        else {
            animator.SetBool("Fallen", cone.isFallen);
        }
	}
}
