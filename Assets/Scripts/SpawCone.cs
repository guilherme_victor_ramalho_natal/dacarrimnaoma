﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawCone : MonoBehaviour {

	public float radius = 1;
	public List<Vector2> centers;
	public GameObject cone;
	public Atacante neymar;

	void Start(){

		foreach (Vector2 center in centers) {
			float xrange = Random.Range (0, radius);
			float yrange = Random.Range (0, radius);
			GameObject newCone = Instantiate (cone);
			newCone.tag = gameObject.tag;
			newCone.transform.position = new Vector3 (center.x + xrange, center.y + yrange, 0);
			newCone.GetComponent<ConeMovement> ().targetNeymar = neymar;
			newCone.GetComponent<Rigidbody2D> ().rotation = Random.Range (0,360);
			if (newCone.tag == "Time2") {
				newCone.transform.localScale = new Vector3 (-1.2f, 1.2f, 1);			
                ConeMovement.vivo2++;
            } else {
                ConeMovement.vivo1++;
            }
		}
	}


	private void OnDrawGizmosSelected(){
		Gizmos.color = Color.red;
		foreach (Vector2 center in centers) {
			Gizmos.DrawSphere(center,radius); 
		}
	}
	
	void Update () {
		
	}
}
