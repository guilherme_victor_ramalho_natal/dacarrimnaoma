﻿ using UnityEngine;

public class Atacante : MonoBehaviour {

    public float startingVelocity;
    public float velocityIncrease;

    public float ballProximity;
    public float ballAttraction;
    public float playerProximity;
    public float maxRepulsion;
    public float maxDistance;

    public Transform opponentNeymar;
    public Transform opponentPlayer;
    public Transform targetGoal;

	private Vector3 getUpPosition;

    public AtacanteState state;
	public bool canDie;

	private float bost;

    float currentVelocity;
    new Rigidbody2D rigidbody;
    Animator animator;

    public void IncreaseVelocity() {
        currentVelocity += velocityIncrease;
    }

    void Awake() {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Start() {
        animator.SetBool("moving", true);
        currentVelocity = startingVelocity;
        state = AtacanteState.FOLLOW_BALL;
    }

    void Update() {
		if (state != AtacanteState.FAKE_FALTA && state != AtacanteState.GETTING_UP ) {
			
			if (DistanceTo (opponentNeymar) < playerProximity && !canDie && opponentNeymar.GetComponent<Atacante>().state != AtacanteState.DOWN) {
				state = AtacanteState.FAKE_FALTA;
                SoundManager.instance.AtivaSomFalta();
				Judge.instance.setTarget (rigidbody.position);
                rigidbody.velocity = Vector2.zero;
				CameraShake.instance.Shake ();
				GetComponent<SpriteRenderer> ().sortingOrder = 0;
				animator.SetTrigger ("fall");
			} 

			else if (DistanceTo (opponentPlayer) < playerProximity && !canDie) {
				state = AtacanteState.FAKE_FALTA;
                SoundManager.instance.AtivaSomFalta();
				Judge.instance.setTarget (rigidbody.position);
                rigidbody.velocity = Vector2.zero;
				CameraShake.instance.Shake ();
				GetComponent<SpriteRenderer> ().sortingOrder = 0;
				animator.SetTrigger ("fall");
				opponentPlayer.GetComponent<PlayerInput> ().lifebar.UpdateLife ();
			} 




			else if(state != AtacanteState.DOWN){
				if (DistanceTo (Ball.instance.position) < ballProximity && Ball.instance.grounded)
					state = AtacanteState.RUN_TOWARDS_GOAL;
               else state = AtacanteState.FOLLOW_BALL;
            }
        }
    }
    
    void FixedUpdate() {
		if (state != AtacanteState.FAKE_FALTA && state != AtacanteState.DOWN) {
			if (state == AtacanteState.FOLLOW_BALL) {
                MoveAvoidingObstacles(Ball.instance.position);
			} else if (state == AtacanteState.RUN_TOWARDS_GOAL) {
                MoveAvoidingObstacles(targetGoal.position);
				Ball.instance.position = transform.position + Direction (transform.position, targetGoal.position) * (0.8f * ballProximity);
			}
			rigidbody.rotation = Mathf.Atan2 (rigidbody.velocity.y, rigidbody.velocity.x) * Mathf.Rad2Deg;
		} if (state == AtacanteState.GETTING_UP) {
			transform.position = Vector3.MoveTowards(transform.position,getUpPosition,Time.deltaTime*bost);
			rigidbody.rotation = Mathf.Atan2 (-(transform.position.y - getUpPosition.y), -(transform.position.x - getUpPosition.x)) * Mathf.Rad2Deg;
			if (getUpPosition == transform.position)
				state = AtacanteState.FOLLOW_BALL;
		}
    }

    void MoveAvoidingObstacles(Vector3 target) {
        var obstacles = Obstacles.instance.GetObstacles();
        Vector3 resultingForce = Direction(transform.position, Ball.instance.position) * ballAttraction;
        foreach (var obstacle in obstacles) {
            if (DistanceTo(obstacle) < maxDistance) {
                float repulsion = maxRepulsion - (maxRepulsion / maxDistance) * DistanceTo(obstacle);
                resultingForce += Direction(obstacle.position, transform.position) * repulsion;
            }
        }
        rigidbody.AddForce(resultingForce);
        if (rigidbody.velocity.magnitude > currentVelocity)
            rigidbody.velocity = rigidbody.velocity.normalized * currentVelocity;
    }

    private float DistanceTo(Transform other) {
        return DistanceTo(other.position);
    }

    float DistanceTo(Vector3 other) {
        return (transform.position - other).magnitude;
    }

    Vector3 Direction(Vector3 from, Vector3 to) {
        return (to - from).normalized;
    }

	public void GetUp(){
		if (state == AtacanteState.FAKE_FALTA) {
			state = AtacanteState.GETTING_UP;
			animator.SetTrigger ("getUp");
			GetComponent<SpriteRenderer> ().sortingOrder = 1;
        }
    }

	public void ToGoal2(){
		getUpPosition = new Vector3 (15, 0, 0);
		bost = 4;
	}

	public void ToGoal1(){
		getUpPosition = new Vector3 (-15, 0, 0);		
		bost = 4;
	}

	void OnTriggerEnter2D(Collider2D collision){
		var player = collision.gameObject.GetComponent<PlayerInput> ();
		if (player) {
			if (canDie){
				state = AtacanteState.DOWN;
				rigidbody.velocity = Vector2.zero;
				CameraShake.instance.Shake ();
				GetComponent<SpriteRenderer> ().sortingOrder = 0;
				animator.SetTrigger ("fall");
			}	
		}
		if (collision.gameObject.tag == "Ball") {
			Ball.instance.isTouchingNeymar = true;
		}
	}

}
