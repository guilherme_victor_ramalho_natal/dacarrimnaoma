﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeBar : MonoBehaviour {


	public Sprite[] sprites = new Sprite[4];
	public int life = 0;

	public int inside = 0; 

	private float alphaInitial;
	private float finalAlpha = 0.5f;
	private Color color;
	public NextScene scene;

	void Start(){
		scene.scene = "End";
		alphaInitial = GetComponent<SpriteRenderer> ().color.a;
		color = GetComponent<SpriteRenderer> ().color;
	}

	public void UpdateLife(){
		life++;
        if (life < 4)
            GetComponent<SpriteRenderer>().sprite = sprites[life];
        if (life >= 3){
            GameObject go = GameObject.Find(tag == "Time1" ? "Player1" : "Player2");
            Judge.instance.setTarget(go.GetComponent<Rigidbody2D>().position, true);
			PlayerPrefs.SetString ("Vitoria", tag == "Time1" ? "player2" : "player1");
			scene.LoadNextScene ();

        }
	}

	void OnTriggerEnter2D(Collider2D collision){
		if(inside == 0)
			StartCoroutine (MakeTransparent(1f));
		inside++;
	}
		
	void OnTriggerExit2D(Collider2D collision){
		inside--;
		if(inside == 0)
			StartCoroutine (MakeSolid(1f));
	}

	private IEnumerator MakeTransparent(float time){
		for (float t = alphaInitial; t > finalAlpha; t -= Time.deltaTime / time) {
			GetComponent<SpriteRenderer> ().color = new Color (color.r,color.g,color.b,t);
			yield return null;
		}
	}

	private IEnumerator MakeSolid(float time){
		for (float t = finalAlpha; t < alphaInitial; t += Time.deltaTime / time) {
			GetComponent<SpriteRenderer> ().color = new Color (color.r,color.g,color.b,t);
			yield return null;
		}

	}

}
