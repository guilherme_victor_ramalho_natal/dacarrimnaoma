﻿using UnityEngine;

public class Judge : MonoBehaviour {

    public static Judge instance { get; private set; }
    private JudgeState state;

	public Atacante atacante1;
	public Atacante atacante2;

    public Goleiro goleiro1;
    public Goleiro goleiro2;

    public PlayerInput player1;
    public PlayerInput player2;

    // Variáis de controle de rotaçã movimento;
    public Vector2 target;
    public Sprite[] spr;
    public Sprite[] sprText;
    public Sprite[] sprDown;
    private float elapsed = 0; // contador de tempo decorrido para "animaçõ" olhar 
    private Quaternion finalRot, inicialRot;
    private Vector2 finalPos, defaultPos, inicialPos;
    public GameObject text;

    private bool gameOver = false;

    public float magnitude;

    void Awake()
    {
        defaultPos = transform.position;
        if (instance)
            Debug.LogError("Cant have more than one Judge at a time");
        instance = this;
    }

    private void Update()
    {

        text.transform.position = ((Vector3.up) * 0.6f) + transform.position;

		if (state != JudgeState.DOWN) {

	        if (state == JudgeState.OLHANDO){
	            Olhar();
	        } else if (state == JudgeState.ANDANDO){
	            Andar();
	        } else if (state == JudgeState.OLHAVOLTA){
	            OlharVoltando();
	        } else if (state == JudgeState.VOLTANDO){
	            Voltar();
	        } else if (state == JudgeState.NORMAL){
	            Idle();
	        } else if (state == JudgeState.AMARELO){
	            Amarelo();
	        } else if (state == JudgeState.VERMELHO){
	            Vermelho();
	        }
        } else {
            Cair();
        }

    }

    public void setTarget(Vector2 pos, bool gameOver = false){
        if (this.gameOver == false){
            target = pos;
            elapsed = 0;
            this.gameOver = gameOver;
            if (state != JudgeState.DOWN)
                state = JudgeState.OLHANDO;
            inicialRot = transform.rotation;
            //Vector2 worldpos = (Vector2)Camera.main.ScreenToWorldPoint(target);

            Vector2 relative = pos - (Vector2)transform.position;
            finalRot = Quaternion.Euler(0, 0, Mathf.Atan2(relative.y, relative.x) * Mathf.Rad2Deg);

            inicialPos = transform.position;
            Vector2 movement = pos - (Vector2)transform.position;
            Vector2 direction = movement.normalized;
            float distance = movement.magnitude - 2;
            finalPos = (Vector2)transform.position + direction * distance;
            text.SetActive(false);
        }
    }

    void Idle(){
        elapsed += Time.deltaTime;
        if ((int)(elapsed * 2) % 2 == 0) {
            gameObject.GetComponent<SpriteRenderer>().sprite = spr[0];
            text.GetComponent<SpriteRenderer>().sprite = sprText[0];
        } else {
            gameObject.GetComponent<SpriteRenderer>().sprite = spr[1];
            text.GetComponent<SpriteRenderer>().sprite = sprText[1];
        }
    }

    void Olhar(){
        
        gameObject.GetComponent<SpriteRenderer>().sprite = spr[2];
        elapsed += Time.deltaTime;
        float t = elapsed / 0.4f;
        t = t * t * t * (t * (6f * t - 15f) + 10f);
        transform.rotation = Quaternion.Lerp(inicialRot, finalRot, t);

        if (t >= 1){
            elapsed = 0;
            state = JudgeState.ANDANDO;
            gameObject.GetComponent<SpriteRenderer>().sprite = spr[2];
        }

    }

    void Andar() {
        elapsed += Time.deltaTime;
        if ((int)elapsed % 2 == 0){
            gameObject.GetComponent<SpriteRenderer>().sprite = spr[2];
        } else {
            gameObject.GetComponent<SpriteRenderer>().sprite = spr[1];
        }

        float time = (inicialPos - finalPos).magnitude/ magnitude;

        float t = elapsed / time;
        //t = t * t * t * (t * (6f * t - 15f) + 10f);
        transform.position = Vector3.Lerp(inicialPos, finalPos, t);
        if (t > 1) {
            elapsed = 0;
            text.SetActive(true);
            // decidir amarelo ou vermelho
            state = gameOver == false ? JudgeState.AMARELO : JudgeState.VERMELHO;
            text.GetComponent<SpriteRenderer>().sprite = state == JudgeState.AMARELO ? sprText[2] : sprText[3];
            // definir caminho de volta.
            inicialPos = transform.position;
            inicialRot = transform.rotation;
            Vector2 relative = defaultPos - (Vector2)transform.position;
            finalRot = Quaternion.Euler(0, 0, Mathf.Atan2(relative.y, relative.x) * Mathf.Rad2Deg);
            inicialPos = transform.position;
            Vector2 movement = defaultPos - (Vector2)transform.position;
            Vector2 direction = movement.normalized;
            float distance = movement.magnitude;
            finalPos = (Vector2)transform.position + direction * distance;
            gameObject.GetComponent<SpriteRenderer>().sprite = spr[2];
        }
    }

    void Amarelo(){
		if (state != JudgeState.DOWN) {
			
			gameObject.GetComponent<SpriteRenderer> ().sprite = spr [3];
			elapsed += Time.deltaTime;
			if (elapsed >= 3) {
				gameObject.GetComponent<SpriteRenderer> ().sprite = spr [2];
				text.SetActive (false);
				elapsed = 0;
				state = JudgeState.OLHAVOLTA;
				text.GetComponent<SpriteRenderer> ().sprite = sprText [0];
                if (player1.fezMerda == true){
                    player1.JudgeLeft();
                }
                if (player2.fezMerda == true){
                    player2.JudgeLeft();
                }
			}
		}
    }

    void Vermelho() {
        if (state != JudgeState.DOWN)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = spr[4];
            elapsed += Time.deltaTime;
            if (elapsed >= 1)
            {
                elapsed = 0;
                gameObject.GetComponent<SpriteRenderer>().sprite = spr[2];
                text.SetActive(false);
                text.GetComponent<SpriteRenderer>().sprite = sprText[0];
                state = JudgeState.OLHAVOLTA;
                gameOver = false; // temp
                if (player1.fezMerda == true)
                {
                    player1.JudgeLeft();
                }
                if (player2.fezMerda == true)
                {
                    player2.JudgeLeft();
                }
            }
        }
    }

    void OlharVoltando(){
        elapsed += Time.deltaTime;
        float t = elapsed / 0.4f;
        t = t * t * t * (t * (6f * t - 15f) + 10f);
        transform.rotation = Quaternion.Lerp(inicialRot, finalRot, t);
        if (t >= 1)
        {
            elapsed = 0;
            state = JudgeState.VOLTANDO;
        }


    }

    void Voltar(){
        text.SetActive(false);
        elapsed += Time.deltaTime;
        if ((int)elapsed % 2 == 0)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = spr[2];
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = spr[1];
        }
        float time = (inicialPos - finalPos).magnitude / magnitude;
        float t = elapsed / time;
        transform.position = Vector3.Lerp(inicialPos, finalPos, t);
        if (t >= 1) {
            elapsed = 0;
            text.SetActive(true);
            state = JudgeState.NORMAL;
			RestoreAtacantes ();
            RestoreGoleiros();
        }
    }

	private void RestoreAtacantes(){
        
		atacante1.GetUp ();
		atacante2.GetUp ();
		atacante1.ToGoal1 ();
		atacante2.ToGoal2 ();
	}

    private void RestoreGoleiros(){
        goleiro1.GetUp();
        goleiro2.GetUp();
    }

	void OnTriggerEnter2D(Collider2D collision){
		var player = collision.gameObject.GetComponent<PlayerInput> ();
		if (player && player.state == PlayerState.CARRINHO) {
			state = JudgeState.DOWN;
            elapsed = 0;
			CameraShake.instance.Shake ();
			Invoke ("NormalState",5);
		}
	}

	private void NormalState(){
		Debug.Log ("enn");
		state = JudgeState.NORMAL;
		if (atacante1.state == AtacanteState.FAKE_FALTA || atacante2.state == AtacanteState.FAKE_FALTA) {
			state = JudgeState.OLHANDO;
		}
	}

    void Cair(){
        text.SetActive(false);
        elapsed += Time.deltaTime;
        if (((int)elapsed*2) % 2 == 0)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = sprDown[0];
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = sprDown[1];
        }
        if (elapsed >= 2)
        {
            elapsed = 0;
            state = JudgeState.VOLTANDO;
            finalPos = defaultPos;
            inicialPos = transform.position;
            Vector2 relative = finalPos - (Vector2)transform.position;
            finalRot = Quaternion.Euler(0, 0, Mathf.Atan2(relative.y, relative.x) * Mathf.Rad2Deg);
            gameObject.GetComponent<SpriteRenderer>().sprite = spr[2];
            RestoreAtacantes();
            RestoreGoleiros();
        }
    }
}


