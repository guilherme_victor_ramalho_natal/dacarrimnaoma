﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour {

    public static Obstacles instance { get; private set; }

    List<Transform> obstacles = new List<Transform>();

    public void AddObstacle(Transform obj) {
        if (!obstacles.Contains(obj))
            obstacles.Add(obj);
    }

    public void RemoveObstacle(Transform obj) {
        if (obstacles.Contains(obj))
            obstacles.Remove(obj);
    }

    public Transform[] GetObstacles() {
        return obstacles.ToArray();
    }

    void Awake() {
        if (instance)
            Debug.LogError("Multiple instances of singleton: Obstacles");
        instance = this;
    }
}
