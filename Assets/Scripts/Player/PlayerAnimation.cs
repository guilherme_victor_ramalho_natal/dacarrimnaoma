﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {

	public Rigidbody2D body;
	new public Animator animation;
	public PlayerInput player;

    void Update()
    {
        if (player.state != PlayerState.OKRAI){
            if (body.velocity != Vector2.zero)
                animation.SetBool("moving", true);
            else
                animation.SetBool("moving", false);

            if (player.state == PlayerState.PULANDO)
                animation.SetTrigger("Jump");
            if (player.state == PlayerState.NORMAL)
            {
                animation.SetTrigger("Land");
                animation.SetTrigger("getUp");
                animation.SetTrigger("JudgeLeft");
            }
            if (player.state == PlayerState.CARRINHO)
                animation.SetTrigger("fall");
            if (player.state == PlayerState.INJURED){
                animation.SetTrigger("down");
            }
        } else if (!animation.GetCurrentAnimatorStateInfo(0).IsTag("Fault")){
            if (animation.GetCurrentAnimatorStateInfo(0).IsName("Fallen"))
            {
                animation.SetTrigger("getUp");
            }
            animation.SetTrigger("JudgeComing");
        }



    }
}
