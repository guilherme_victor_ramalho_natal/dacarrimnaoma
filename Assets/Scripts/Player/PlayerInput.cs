﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

	public Rigidbody2D body;
	public PlayerState state; 
	public float magnitude;
	public LifeBar lifebar;
	public float cooldownTime;
    public Vector2 fieldLimits;
    public bool fezMerda = false;

    private Dictionary<string, string> player1Control = new Dictionary<string, string>{
        {"horizontal","Horizontal1"},{"vertical","Vertical1"},
        {"carrinho","Carrinho1"},{"jump","Jump1"}
    };
    private Dictionary<string, string> player2Control = new Dictionary<string, string>{
        {"horizontal","Horizontal2"},{"vertical","Vertical2"},
        {"carrinho","Carrinho2"},{"jump","Jump2"}
    };

    private Dictionary<string,string> myControl;
	private bool stoped = false;
	private bool cooldown;
	private bool freeze;
    private float scale;
    private Vector2 carrinhoSpeed;

	void Start(){
		cooldown = false;
		freeze = false;
		if (gameObject.tag == "Time1")
			myControl = player1Control;
		else
			myControl = player2Control;
        scale = transform.localScale.x;
	}

	void FixedUpdate () {
		if (state != PlayerState.INJURED) {
			Move ();
			Act ();
			LookTo ();
            FitInsideField();
		}
	}

    void FitInsideField() {
        var position = transform.position;
        position.x = Mathf.Clamp(position.x, -fieldLimits.x, fieldLimits.x);
        position.y = Mathf.Clamp(position.y, -fieldLimits.y, fieldLimits.y);
        transform.position = position;
    }

	private void LookTo(){
		if (body.velocity.magnitude > 0.2f) {
			float angle = Mathf.Atan2 (body.velocity.y, body.velocity.x);
			gameObject.transform.rotation = Quaternion.Euler (0,0,Mathf.Rad2Deg*angle);
		}
	}

	private void Act(){
        if (state == PlayerState.NORMAL && Input.GetAxis(myControl["carrinho"]) != 0 && !cooldown && !freeze)
        {
            DaCarrinho();
        }
        else if (state == PlayerState.NORMAL && Input.GetAxis(myControl["jump"]) > 0.1f)
        {
            Jump();
        }
	}


	private void Jump(){
		GetComponent<Collider2D> ().enabled = false;
		state = PlayerState.PULANDO;
		GetComponent<SpriteRenderer> ().sortingOrder = 2;
		Invoke ("Grow", 0.1f);
		Invoke ("Grow", 0.2f);
		Invoke ("Grow", 0.3f);
		Invoke ("Grow", 0.4f);
		Invoke ("Srink",0.5f);
		Invoke ("Srink",0.6f);
		Invoke ("Srink",0.7f);
		Invoke ("Srink",0.8f);
		Invoke ("Normal",1);
	}

	private void Grow(){
		scale = scale*1.08f;
		gameObject.transform.localScale = new Vector3 (scale,scale);
	}

	private void Srink(){
		scale = scale/1.08f;
		gameObject.transform.localScale = new Vector3 (scale,scale);
	}

	private void DaCarrinho(){
		cooldown = true;
		state = PlayerState.CARRINHO;
		body.velocity = body.velocity * 2f;
        carrinhoSpeed = body.velocity;
		Invoke ("Stop", 0.4f);
		Invoke ("Normal",0.5f);
		Invoke ("RefreshCooldown", cooldownTime);
	}

	private void RefreshCooldown(){
		cooldown = false;
	}

	private void Normal(){
		GetComponent<SpriteRenderer> ().sortingOrder = 1;
		GetComponent<Collider2D> ().enabled = true;
		GetComponent<Collider2D> ().isTrigger = false;
        if(state != PlayerState.OKRAI){
            state = PlayerState.NORMAL;
            stoped = false;
        }
	}

	private void Stop(){
		body.velocity = Vector2.zero;
		stoped = true;
	}

	private void Move(){
		DirectionMove ();
		if (state == PlayerState.CARRINHO) {
			body.velocity = body.velocity * magnitude * 2;
		}
        else if (state == PlayerState.PULANDO) {
            body.velocity = body.velocity * magnitude * 1.2f;
        }
        else if (!stoped){
			body.velocity = body.velocity * magnitude;
		}
	}

	private void DirectionMove(){
        if (state != PlayerState.OKRAI){
            
        }
        if (state != PlayerState.CARRINHO)
                body.velocity = new Vector2(Input.GetAxis(myControl["horizontal"]), Input.GetAxis(myControl["vertical"]));
            else body.velocity = carrinhoSpeed;
            body.velocity = body.velocity.normalized;
	}

	void OnCollisionEnter2D(Collision2D collider){
		var player = collider.gameObject.GetComponent<PlayerInput> ();
		if (player) {
			if (player.state == PlayerState.CARRINHO) {
				GetComponent<Collider2D> ().isTrigger = true;
				body.velocity = Vector2.zero;
				state = PlayerState.INJURED;
				CameraShake.instance.Shake ();
				GetComponent<SpriteRenderer> ().sortingOrder = 0;
				Invoke ("Normal", 2);
			}
		}
	}

	void OnCollisionStay2D(Collision2D collider){
		var cone = collider.gameObject.GetComponent<ConeMovement> ();
		if (cone) {
			freeze = true;
			body.velocity = Vector2.zero;
		}
	}

	void OnCollisionExit2D(Collision2D collider){
		var cone = collider.gameObject.GetComponent<ConeMovement> ();
		if (cone) 
			freeze = false;
	}

    public void JudgeComing(){
        fezMerda = true;
        Stop();
        state = PlayerState.OKRAI;
    }

    public void JudgeLeft(){
        Normal();
        fezMerda = false;
        state = PlayerState.NORMAL;
    }
}
