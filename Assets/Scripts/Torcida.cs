﻿using UnityEngine;

public class Torcida : MonoBehaviour {

    public float minVol;
    public float minVolDistance;
    public float maxVolDistance;
    public float updateVel;
    public AudioSource soundtrackSource;
    public float maxSoundtrackVolume;
    public float minSoundtrackVolume;

    AudioSource source;

    void Awake() {
        source = GetComponent<AudioSource>();
    }

    void Update() {
        float targetVolume, targetSoundtrackVolume;
        if (Ball.instance != null) {
            float ballX = Mathf.Abs(Ball.instance.position.x);
            if (ballX < minVolDistance) {
                targetVolume = minVol;
                targetSoundtrackVolume = maxSoundtrackVolume;
            }
            else if (ballX < maxVolDistance) {
                targetVolume = Mathf.Lerp(minVol, 1f, (ballX*ballX) / (maxVolDistance*maxVolDistance));
                targetSoundtrackVolume = Mathf.Lerp(maxSoundtrackVolume, minSoundtrackVolume, (ballX * ballX) / (maxVolDistance * maxVolDistance)); ;
            }
            else {
                targetVolume = maxVolDistance;
                targetSoundtrackVolume = minSoundtrackVolume;
            }
        }
        else {
            targetVolume = minVol;
            targetSoundtrackVolume = maxSoundtrackVolume;
        }
        source.volume = Mathf.Lerp(source.volume, targetVolume, updateVel);
        soundtrackSource.volume = Mathf.Lerp(soundtrackSource.volume, targetSoundtrackVolume, updateVel);
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(0, 1), new Vector3(minVolDistance, 1));
        Gizmos.DrawLine(new Vector3(0, -1), new Vector3(maxVolDistance, -1));
    }
}
